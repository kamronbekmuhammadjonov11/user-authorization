const config = require("config")
const jwt = require("jsonwebtoken")

function auth(req, res, next) {
    const token = req.header("x-auth-token")
    if (!token) {
        return res.status(401).send("Unauthorized")
    }

    try {
        req.user = jwt.verify(token, config.get("jwtPrivateKey"));
        next()
    } catch (err) {
        return res.status(400).send("Token is wrong")
    }
}

module.exports = auth
