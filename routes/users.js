const express = require("express");
const bcrypt = require("bcrypt")
const _ = require("lodash");
const {validate, User} = require("../models/user");
const Joi = require("joi");
const router = express.Router();
const jwt = require("jsonwebtoken");
const config = require("config")

router.post("/register", async (req, res) => {

    const {error} = validate(req.body);
    if (error) {
        return res.status(400).send(error);
    }

    const findUser = await User.findOne({email: req.body.email})
    if (findUser) {
        return res.status(400).send("Such a user exist")
    }

    const user = new User(req.body);
    const salt = await bcrypt.genSalt();
    user.password = await bcrypt.hash(user.password, salt);
    const savedUser = await user.save();

    res.status(201).send(_.pick(savedUser, ["_id", "name", "email"]));


})

router.post("/login", async (req, res) => {

    const {error} = validateUserForUser(req.body);

    if (error) {
        return res.status(400).send(error);
    }

    const findUser = await User.findOne({email: req.body.email})
    if (!findUser) {
        return res.status(400).send("Email or password is wrong")
    }

    const isValidPassword = await bcrypt.compare(req.body.password, findUser.password);

    if (!isValidPassword) {
        return res.status(400).send("Email or password is wrong");
    }

    const token = findUser.generateAuthToken()

    res.status(200).header("x-auth-token", token).send(true);


})


function validateUserForUser(user) {
    const schema = Joi.object(({
        email: Joi.string().min(5).max(255).required().email(),
        password: Joi.string().min(8).max(1024).required()
    }));
    return schema.validate(user);

}


module.exports = router
