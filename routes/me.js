const express = require("express");
const {User} = require("../models/user");
const router = express.Router();
const auth = require("../middleware/auth");

router.get("/", auth, async (req, res) => {
    throw new Error("Get user profile error")
    const user = await User.findById(req.user._id).select("-password")
    res.send(user)
})


module.exports = router
