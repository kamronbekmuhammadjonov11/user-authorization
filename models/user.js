const {Schema, model} = require("mongoose");
const Joi = require("joi");
const jwt = require("jsonwebtoken");
const config = require("config");


const userSchema = new Schema({
    name: {
        required: true,
        type: String,
        minLength: 3,
        maxLength: 50
    },
    email: {
        required: true,
        unique: true,
        type: String,
        minLength: 5,
        maxLength: 255,
    },
    password: {
        required: true,
        type: String,
        minLength: 8,
        maxLength: 1024
    }
})

userSchema.methods.generateAuthToken = function () {
    return  jwt.sign({_id: this._id}, config.get("jwtPrivateKey"))
}

const User = model("User", userSchema);

function validateUser(user) {
    const schema = Joi.object(({
        name: Joi.string().min(3).max(50).required(),
        email: Joi.string().min(5).max(255).required().email(),
        password: Joi.string().min(8).max(1024).required()
    }));
    return schema.validate(user);

}

exports.User = User;
exports.validate = validateUser;

