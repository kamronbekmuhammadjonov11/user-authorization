require("express-async-errors");
const winston = require("winston");
require("winston-mongodb");

module.exports = function () {
    winston.add(new winston.transports.Console())
    winston.add(new winston.transports.File({filename: "User-authorization-logs.log"}));
    winston.add(new winston.transports.MongoDB({db: "mongodb://localhost/users-logs"}))

    winston.exceptions.handle(new winston.transports.Console(), new winston.transports.File({filename: "User-authorization-logs.log"}))
    process.on("unhandledRejection", (ex) => {
        throw ex;
    })

}
