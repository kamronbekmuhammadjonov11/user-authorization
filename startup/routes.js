const express = require("express");
const me = require("../routes/me");
const users = require("../routes/users");
const errorMiddleware = require("../middleware/error");
module.exports = function (app) {
    app.use(express.json())
    app.use(express.urlencoded({extended: true}));
    app.use("/me", me);
    app.use("/api/users", users);
    app.use(errorMiddleware)
}
