const mongoose = require("mongoose");
const winston = require("winston");


module.exports = function () {
    mongoose.connect("mongodb://localhost/users").then(() => {
        winston.debug("Mongoose is working")
    }).catch(() => {
        winston.error("Mongoose is not working")
    })
}
