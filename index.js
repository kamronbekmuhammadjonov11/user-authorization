const express = require("express");
const app = express();
const winston = require("winston")

require("./startup/logging")();
require("./startup/routes")(app);
require("./startup/db")();
require("./startup/config")();

app.listen(5000, () => {
    winston.info("I started list to 5000 port");
})




